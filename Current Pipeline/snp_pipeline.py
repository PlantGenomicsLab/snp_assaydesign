# -*- coding: utf-8 -*-
"""
Created on Mon Sep  9 09:38:25 2019

@author: avafr
"""
#!/usr/bin/env python

import io
import pandas as pd

usage = print("usage: snp_pipeline flanking_region_length preserved_length [-platform] vcf1 [-vcf2] [-iupac] [-snp_location] [-gtf] [-indels] [-both]")
vcf1 = input("What is the name of your main VCF file? : ")
opt_vcf2 = input("Would you like to input a second vcf? True or False : ")
if opt_vcf2 == str("True"):
    choose_vcf2 = input("What is the name of your second VCF file? : ")
else:
    pass
opt_platform = input("Would you like to choose a platform? True or False : ")
if opt_platform == str("True"):
    choose_plat = input("What platform would you like to follow? Thermo or Test1 : ")
    if choose_plat == str("Thermo"):
        flanking_region_length = 35
    else:
        flanking_region_length = 1
else:
   flanking_region_length = int(input("What is your flanking length? : "))
   preserved_length = int(input("What is your preserved length? : "))
   opt_iupac = input("Would you like to follow IUPAC? True or False : ")
   if opt_iupac == str("True"):
       pass
   else:
       pass
   opt_snp_location = input("Would you like to identify a SNP region? True or False : ")
   if opt_snp_location == str("True"):
       pass
   else:
       pass
def read_vcf(path):
    with open(path, 'r') as f:
        lines = [l for l in f if not l.startswith('##')]
    return pd.read_csv(
        io.StringIO(''.join(lines)),
        dtype={'#CHROM': str, 'POS': int, 'ID': str, 'REF': str, 'ALT': str,
               'QUAL': str, 'FILTER': str, 'INFO': str},
        sep='\t'
    ).rename(columns={'#CHROM': 'CHROM'})

df = (read_vcf(vcf1))
print(df)
df['same_chrom'] = df['CHROM'].eq(df['CHROM'].shift())
indexPOS = df[(df['POS'].diff().between(0,flanking_region_length))].index
indexCHROM = df.loc[df['same_chrom']==True].index
df.drop(indexPOS & indexCHROM, inplace=True)
df.pop('same_chrom')
print(df)